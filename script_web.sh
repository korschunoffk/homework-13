#!/bin/bash

yum install rsyslog -y
systemctl enable rsyslog

cat <<EOT > /etc/yum.repos.d/nginx.repo
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/\$releasever/\$basearch/
gpgcheck=0
enabled=1
EOT

yum install nginx -y
systemctl enable nginx
systemctl stop nginx

sed -i 's/access_log/#access_log/g' /etc/nginx/nginx.conf
sed -i 's/error.log warn;/error.log crit;/g' /etc/nginx/nginx.conf
sed -i '6 i error_log syslog:server=192.168.11.102:514,tag=nginxerror debug;' /etc/nginx/nginx.conf
sed -i '24 i     access_log syslog:server=192.168.11.102:514,tag=nginxaccess main;' /etc/nginx/nginx.conf

systemctl start nginx
setenforce permissive
auditctl -w /etc/nginx/ -k test_watch

cat <<EOT > /etc/rsyslog.d/audit.conf
\$ModLoad imfile
\$InputFileName /var/log/audit/audit.log
\$InputFileTag tag_audit_log:
\$InputFileStateFile audit_log
\$InputFileSeverity info
\$InputFileFacility security
\$InputRunFileMonitor

security.*   @192.168.11.102:514
EOT

systemctl restart rsyslog


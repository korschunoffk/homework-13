#!/bin/bash
yum install rsyslog -y
systemctl enable rsyslog
systemctl start rsyslog

sed -i 's/#$ModLoad imudp/$ModLoad imudp/g' /etc/rsyslog.conf
sed -i 's/#$UDPServerRun 514/$UDPServerRun 514/g' /etc/rsyslog.conf

sed -i '22 i $template RemoteLogs,"/var/log/%HOSTNAME%/%PROGRAMNAME%.log"' /etc/rsyslog.conf
sed -i '23 i *.* -?RemoteLogs' /etc/rsyslog.conf

sed -i '24 i $template HostAudit,"/var/log/%HOSTNAME%/audit.log"' /etc/rsyslog.conf
sed -i '25 i security.* -?HostAudit' /etc/rsyslog.conf

systemctl restart rsyslog
setenforce permissive




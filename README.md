#Homework 13 - Центральный сервер логов
Было принято решение использовать RSyslog.
Подняты 2 машины - 
web `192.168.11.101`
log `192.168.11.102`

**Provisioning script web**


```
yum install rsyslog -y                                                                                      Устновка rsyslog
systemctl enable rsyslog    

cat <<EOT > /etc/yum.repos.d/nginx.repo                                                                     Добавляем репо nginx и устанавливам 
[nginx]
name=nginx repo
baseurl=http://nginx.org/packages/centos/\$releasever/\$basearch/
gpgcheck=0
enabled=1
EOT

yum install nginx -y
systemctl enable nginx
systemctl stop nginx
                                                                                                            Настраиваем отправку логов на удаленный сервер
sed -i 's/access_log/#access_log/g' /etc/nginx/nginx.conf                                                   Отключаем запись access логов локально
sed -i 's/error.log warn;/error.log crit;/g' /etc/nginx/nginx.conf                                          Оставляем локально только error логи уровня critical
sed -i '6 i error_log syslog:server=192.168.11.102:514,tag=nginxerror debug;' /etc/nginx/nginx.conf         Отправляем на удаленный сервер error логи уровня debug и выше, разделяем с помощью tag
sed -i '24 i     access_log syslog:server=192.168.11.102:514,tag=nginxaccess main;' /etc/nginx/nginx.conf   Отправляем на удаленный сервер access логи уровня main и выше, разделяем с помощью tag

systemctl start nginx
setenforce permissive
auditctl -w /etc/nginx/ -k test_watch                                                                       Добавляем к аудиту папку /etc/nginx/

cat <<EOT > /etc/rsyslog.d/audit.conf                                                                       Создаем конфигурационный файл для передачи 
\$ModLoad imfile                                                                                            audit.log на сервер логов
\$InputFileName /var/log/audit/audit.log
\$InputFileTag tag_audit_log:
\$InputFileStateFile audit_log
\$InputFileSeverity info
\$InputFileFacility security                                                                                Пересылаем только facility security
\$InputRunFileMonitor

security.*   @192.168.11.102:514                                                                            на лог сервер на 514 порт
EOT

systemctl restart rsyslog
```
**Provisioning script log**

```
yum install rsyslog -y                                                                                      Устанавливаем rsyslog
systemctl enable rsyslog
systemctl start rsyslog

sed -i 's/#$ModLoad imudp/$ModLoad imudp/g' /etc/rsyslog.conf                                               Загружаем модуль работающий с udp 
sed -i 's/#$UDPServerRun 514/$UDPServerRun 514/g' /etc/rsyslog.conf                                         Открываем UDP порт 514

sed -i '22 i $template RemoteLogs,"/var/log/%HOSTNAME%/%PROGRAMNAME%.log"' /etc/rsyslog.conf                Вносим шаблон для приема логов access и error 
sed -i '23 i *.* -?RemoteLogs' /etc/rsyslog.conf

sed -i '24 i $template HostAudit,"/var/log/%HOSTNAME%/audit.log"' /etc/rsyslog.conf                         Вносим шаблон для приема логов audit
sed -i '25 i security.* -?HostAudit' /etc/rsyslog.conf

systemctl restart rsyslog               
setenforce permissive                                                                                       Хотя лекция по SE уже была, я еще не разбирался и пока вопрос решил такой портянкой
```

Проверить работу можно внеся какое либо изменение в папку /etc/nginx/ на сервере web
и выполнив curl http://192.168.11.101:80 

Соответствующие записи появятся на сервере логов в папке /var/log/web 
